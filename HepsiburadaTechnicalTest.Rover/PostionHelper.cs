﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HepsiburadaTechnicalTest.Rover
{
    public class PostionHelper
    {
        public int _x { get; set; } // Rover x
        public int _y { get; set; } // Rover y
        private int _rowMax;
        private int _colMax;
        public string _roverDirection; // Rover direction
        string[] _roverDirectionArr = { "N", "E", "S", "W" };

        public PostionHelper(string loc, int rowMax, int colMax)
        {
            string[] str = loc.Split(" ");
            this._x = Int32.Parse(str[0]);
            this._y = Int32.Parse(str[1]);
            _roverDirection = str[2];
            this._rowMax = rowMax;
            this._colMax = colMax;
        }

        public void TurnLeft()
        {
            int index = Array.IndexOf(_roverDirectionArr, _roverDirection);
            if (index > -1 && index < _roverDirectionArr.Length)
            {
                _roverDirection = _roverDirectionArr[(index - 1 + _roverDirectionArr.Length) % _roverDirectionArr.Length];
            }

        }

        public void TurnRight()
        {
            int index = Array.IndexOf(_roverDirectionArr, _roverDirection);
            if (index > -1 && index < _roverDirectionArr.Length)
            {
                _roverDirection = _roverDirectionArr[(index + 1) % _roverDirectionArr.Length];
            }

        }

        public void Move()
        {
            switch (_roverDirection)
            {
                case "N":
                    if (_y < _rowMax)
                        _y = _y + 1;
                    break;
                case "W":
                    if (_x > 0)
                        _x = _x - 1;
                    break;
                case "S":
                    if (_y > 0)
                        _y = _y - 1;
                    break;
                case "E":
                    if (_x < _colMax)
                        _x = _x + 1;
                    break;
                default:

                    break;
            }
        }
        public void Move(string Messages)
        {
            char[] messages = Messages.ToCharArray();

            for (int i = 0; i < messages.Length; i++)
            {
                switch (messages[i])
                {
                    case 'L':
                        TurnLeft();
                        break;
                    case 'R':
                        TurnRight();
                        break;
                    case 'M':
                        Move();
                        break;
                    default:

                        break;
                }
            }
        }
    }
}
