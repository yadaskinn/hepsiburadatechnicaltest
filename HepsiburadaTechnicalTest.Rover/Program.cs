﻿using System;

namespace HepsiburadaTechnicalTest.Rover
{
    class Program
    {
        static void Main(string[] args)
        {
            string maxPoints = "5 5";
            string firstPositions = "1 2 N";
            string firstMoves = "LMLMLMLMM";
            string secondPositions = "3 3 E";
            string secondMoves = "MMRMMRMRRM";

            string[] str = maxPoints.Split(" ");
            int row = Int32.Parse(str[0]);
            int col = Int32.Parse(str[1]);

            PostionHelper fisrtCallRover = new PostionHelper(firstPositions, row, col);
            fisrtCallRover.Move(firstMoves);
            Console.WriteLine(fisrtCallRover._x + " " + fisrtCallRover._y + " " + fisrtCallRover._roverDirection);

            PostionHelper secondCallRover = new PostionHelper(secondPositions, row, col);
            secondCallRover.Move(secondMoves);
            Console.WriteLine(secondCallRover._x + " " + secondCallRover._y + " " + secondCallRover._roverDirection);
            Console.ReadLine();
        }
    }
}
